import {getData} from '../server/index'
export default {
    namespace: 'detectionresult',
    state:{
        name:'zys'
    },
    effects:{
       *saveState(action,{call,put}){
         let res=yield call(getData,action)
         yield put({
             type:'saveRedus',
             payload:res
         })
       }
    },
    reducers:{
       saveRedus(state,{payload}){
           return {
               ...state,
               list:payload
           }
       }
    }
}