export default {
    OVER_ALL_SITUATION: "整体态势",
    CON_TENT_DETEC_TION:"内容检测",
    GROU_PDETE_CTION:"群组检测",
    ASS_OCIAT_ION_ANA_LYSIS:"关联分析",
    ANA_LY_SIS:"监测配置",
    COLL_ECTION_CONFIG_URATION: "采集配置",
    SYS_TEMCONFIGURAT_ION:"系统配置",
    EMER_GEN_CY:"突发预警系统"
};