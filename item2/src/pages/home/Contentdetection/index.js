import React, { Component } from 'react'
import './index.css';
import Select from './component/Select';
import {Icon} from 'antd'
import Link from 'umi/link'
 class ContentDetection extends Component {
  render() {
    return (
      <div className='content-Box'>
         <header className='header'><p className='m-content-room'><Icon type='home'/>埃塞尔比亚坠机</p><Select name='全部账号'/><Select name='全部群主'/></header>
     <div className="tab-header">
      <Link to='/home/Contentdetection/detectionresult' className="m-tab-header">检测结果</Link>
      <Link to='/home/Contentdetection/detection' className="m-tab-header">检测分析</Link>
     </div>
      {this.props.children}
      </div>
    )
  }
}
export default ContentDetection