import React, { Component } from 'react'
import {connect} from 'dva'
import {List} from 'antd'
import './dec.css'
class Detectionresult extends Component {
    state={
        list:[]
    }
    componentDidMount(){
        this.props.dispatch({
            type: "detectionresult/saveState",
          })
    }
  render() {
     
      let content=this.props.list && this.props.list.data.data
     return (
      <div>
       <List
          size="large"
          header={<div>列表展示</div>}
          footer={<div>Footer</div>}
          bordered
          dataSource={content}
          renderItem={item => (
            <div className="item">
              <h3>{item.describe}</h3>
              <p>
                <span>时间: {item.time}</span>
                <span>来源: <i>{item.source}</i></span>
              </p>
              <p>
                <span>{item.country}</span>
                <span>{item.type}</span>
              </p>
            </div>
          )}
        />
      </div>
    )
  }
}
export default connect((state)=>{
   return {
       ...state.detectionresult
   }
})(Detectionresult)