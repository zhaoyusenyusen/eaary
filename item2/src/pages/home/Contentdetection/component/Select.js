import React, { Component } from 'react'
import { Select } from 'antd';
const { Option } = Select;
class Selects extends Component {
handleChange=(value)=> {
    console.log(`selected ${value}`);
}
 render() {
    return (
      <div>
         <Select defaultValue={this.props.name} className='select' onChange={this.handleChange}>
                <Option value="option1" className='option'>option1</Option>
                <Option value="option2" className='option'>option2</Option>
                <Option value="option3" className='option'>option3</Option>
                <Option value="option4" className='option'>option4</Option>
        </Select>
     </div>
    )
  }
}
export default Selects