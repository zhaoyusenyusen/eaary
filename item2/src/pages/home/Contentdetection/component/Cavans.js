import React from 'react'
import {Button} from 'antd'
 class Canvas extends React.Component {
     state={
         flage:true
     }
  componentDidMount () {
     this.progess()
  }
  progess(){
var canvas = document.getElementById('canvas')
var ctx = canvas.getContext('2d'),
  deg = Math.PI / 180;
ctx.translate(250, 250); //重定向原点
// 绘制线条代码封装
function line(x1, y1, x2, y2, color) {
  ctx.beginPath();
  ctx.strokeStyle = color;
  ctx.font = "16px 微软雅黑"
  ctx.moveTo(x1, y1);
  ctx.lineTo(x2, y2)
  ctx.closePath();
  ctx.stroke()
}
// 绘制文字
function text(txt, x, y, size, color) {
  ctx.beginPath();
  ctx.textAlign = "center";
  ctx.font = `${size} 微软雅黑`;
  ctx.fillStyle = color;
  ctx.fillText(txt, x, y)
  ctx.closePath();
  ctx.stroke()
}
//绘制刻度线
function drawLine(color) {
  for (var i = 0; i <= 100; i++) {
    var x1 = Math.cos((150 + 2.4 * i) * deg) * 200,
      y1 = Math.sin((150 + 2.4 * i) * deg) * 200,
      x2 = Math.cos((150 + 2.4 * i) * deg) * 210,
      y2 = Math.sin((150 + 2.4 * i) * deg) * 210;

    if (i % 10 === 0) {
      x2 = Math.cos((150 + 2.4 * i) * deg) * 220;
      y2 = Math.sin((150 + 2.4 * i) * deg) * 220;
      var x3 = Math.cos((150 + 2.4 * i) * deg) * 180,
        y3 = Math.sin((150 + 2.4 * i) * deg) * 180;
      text(i, x3, y3, "16px", color)
    }
    line(x1, y1, x2, y2, color);


  }

}

function color() {
  var x1 = Math.cos((150) * deg) * 180,
    y1 = Math.sin((150) * deg) * 180,
    x2 = Math.cos((150 + 2.4 * 100) * deg) * 210,
    y2 = Math.sin((150 + 2.4 * 100) * deg) * 210;
  var grd = ctx.createLinearGradient(x1, y1, x2, y2);
  grd.addColorStop(0, 'green');
  grd.addColorStop(0.4, 'blue');
  grd.addColorStop(0.8, 'orange');
  grd.addColorStop(1, 'red');
  return grd;
}

drawLine("#222")
var end = 0;
draw()
let that=this
function draw() {
  ctx.clearRect(-100, -100, 200, 200);
  drawLine(color())
  arc(end, "red")
  text(end + "%", 0, 0, "60px", color());
  if (end < 100) {
   window.requestAnimationFrame(draw)
  
  }
  end++;
}

function arc(i, color) {
var x11 = Math.cos((150 + 2.4 * i) * deg) * 226,
    y11 = Math.sin((150 + 2.4 * i) * deg) * 226;
  ctx.beginPath();
  ctx.fillStyle = color;
  ctx.arc(x11, y11, 4, 0, 360 * deg)
  ctx.closePath()
  ctx.fill()

  var x12 = Math.cos((150 + 2.4 * (i - 1)) * deg) * 226,
    y12 = Math.sin((150 + 2.4 * (i - 1)) * deg) * 226;
  ctx.beginPath();
  ctx.fillStyle = "#eee";
  ctx.arc(x12, y12, 5, 0, 360 * deg)
  ctx.closePath()
  ctx.fill()
}
  }
  addClick(){
     this.setState({
         flage:this.state.flage
     })
  }
  render () {
    return (
      <div>
        
        <canvas id="canvas" width="500" height="500" />
         <Button onClick={()=>this.addClick()}>点击</Button>
        
      </div>
    )
  }
}
export default Canvas
