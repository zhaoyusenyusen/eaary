import React, { Component } from 'react';

// 引入 ECharts 主模块
import echarts from 'echarts/lib/echarts';
// 引入柱状图
import  'echarts/lib/chart/bar';
// 引入提示框和标题组件
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';

class EventType extends Component {
    componentDidMount() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main'));
        // 绘制图表
        myChart.setOption({
            title: {},
            tooltip: {},
            xAxis: {
                data: ["爆炸品", "枪支交易", "违禁药品", "情报交易", "数据黑市", "信息泄露"]
            },
            yAxis: {},
            series: [{
                name: '销量',
                type: 'bar',
                itemStyle: {
                normal: {
                color: function (params) {
                var colorList = [
                        'orange', 'skyblue', 'pink', 'green', 'red', '#32c5d2', '#67b7dc'
                      ];
                return colorList[params.dataIndex]
                 },
                   }
                 },
                data: [35, 28, 21, 13, 5, 35]
            }],
            
        });
    }
    render() {
        return (
            <div id="main" style={{ width: 500, height: 400,background:'#fff',marginTop:10 }}></div>
        );
    }
}

export default EventType;
