import React, { Component } from 'react'
import io  from 'socket.io-client'
import './scoket.css'
const socket = io('ws://localhost:4000')
class Hotscoket extends Component {
    state={
        arrs:[]
    }
    componentDidMount(){
    let that=this
    socket.on('news', function (data) {
    that.setState({
       arrs:data.data
   })
 })
 }
  render() {
      let {arrs}=this.state
    return (
      <div className='Hotscoket'>
        <ol className='box-li'>
             <h3>热点新闻</h3>
            <li>{arrs.tit?arrs.tit:'You have 4 pending tasks.'}</li>
            <li>{arrs.news?arrs.news:'新闻联播'}</li>
            <li>{arrs.shop?arrs.shop:'5G上线'}</li>
            <li>{arrs.umi?arrs.umi:'react 约定是框架不会用'}</li>
            <li>{arrs.b?arrs.b:'美国欢迎中国学生  释放啥信号?  媒体:希望落到实处'}</li>
            <li>{arrs.c?arrs.c:'代码太难了!'}</li>
            <li>{arrs.a?arrs.a:'民航局回应桂林航空“女乘客进驾驶舱”事件'}</li>
            <li>{arrs.f?arrs.f:'北京装配式建筑迎来大发展 业内聚焦“人才先行”'}</li>
            <li>{arrs.g?arrs.g:'张善政担任韩国瑜参选副手 誓言不让蔡英文连任”'}</li>
            <li>{arrs.d?arrs.d:'明星反季节工作画面太美，孙俪十分敬业了”'}</li>
        </ol>
      </div>
    )
  }
}
export default Hotscoket