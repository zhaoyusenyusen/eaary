import React from 'react'
import { Layout, Menu, Icon ,Input,Select} from 'antd';
import styles from './index.css'
import Link from 'umi/link';
import { IntlProvider,FormattedMessage } from "react-intl";
import zhTW from "../locales/en-US";
import zhCN from '../locales/zh-CN';
import axios from 'axios'
const { Option} = Select;
const { Header, Sider, Content } = Layout;
class SiderDemo extends React.Component {
     constructor(props){
        super(props)
         this.state = {
           collapsed: false,
           flage:true,
           arrs:[],
           loading: true

         }
     }
toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };
handleChange=(value)=> {
  if(value==="Elinges"){
     this.setState({
         flage: !this.state.flage
     })
  }else{
       this.setState({
         flage: !this.state.flage
       })
  }
}
  componentDidMount() {
      axios.get('http://localhost:4000/router').then(res => {
        if(res.data.code===1){
            this.setState({
                arrs:res.data.data,
                loading:false
            })
          }
      })
    }


  render() {
  let {arrs}=this.state
    return (
         
         <IntlProvider messages={this.state.flage?zhCN:zhTW}  locale={navigator.language}>
        <div>
        <Layout>
        <Sider trigger={null} collapsible collapsed={this.state.collapsed} style={{ background:"#526b86", padding: 0 }} >
            <Icon
              onClick={this.toggle}
              className={this.state.collapsed?styles.Headericons:styles.icons}
              type = {
              this.state.collapsed ? 'menu-unfold' : 'menu-fold'
            }/>
       <h3 className={this.state.collapsed?styles.h3:''} className={this.state.collapsed?'':styles.h3title}>{this.state.collapsed?'': < FormattedMessage id = 'EMER_GEN_CY' > 突发预警系统 </FormattedMessage>}</h3>    
         
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}  style={{background:"#526b86",paddingTop: 10}}>
            {arrs&&arrs.map((item)=>{
               return <Menu.Item key={item.byId}>
                <Icon type="user"/>
                <FormattedMessage id = {item.id} values={{name:item.tit}}> {item.tit} </FormattedMessage>
                <Link to={item.link}></Link>
                 </Menu.Item>
             })}      
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background:"#526b86", padding: 0 }}>
             <span>
             <Icon
              className={this.state.collapsed?styles.trigger:styles.triggers}
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
            />
            </span>
              <Input type="text" placeholder="Type here..."  style={{ width:132,height:35, padding: 0,marginLeft:850 }}/>
              <Select defaultValue="中文"  onChange={this.handleChange} style={{ width:100,height:60,marginLeft:180 }}>
             <Option value="English">English</Option>
             <Option value="中文">中文</Option>
           </Select>
          </Header>
          <Content
            style={{
              margin: '24px 16px',
              padding: 24,
              background: '#eee',
              minHeight: 880,
            }}
          >
        {this.props.children}
        </Content>
        </Layout>
      </Layout>
     </div>
    </IntlProvider>
   
    );
  }
}
export default SiderDemo