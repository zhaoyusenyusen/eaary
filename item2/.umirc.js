
// ref: https://umijs.org/config/
export default {
    treeShaking: true,
    routes: [
        {
            path: '/',
            component: './home/index.js',
            routes: [{
                path: '/',
                component: './home/Overallsituation/index.js',
            },
            {
                path: '/home/Overallsituation',
                component: './home/Overallsituation/index.js',
            },
            {
                path: '/home/Contentdetection',
                component: './home/Contentdetection/index.js',
                routes: [
                     {
                       path: '/home/Contentdetection',
                       component: './home/Contentdetection/detectionresult/index.js',
                     },
                    {
                    path: '/home/Contentdetection/detectionresult',
                    component: './home/Contentdetection/detectionresult/index.js',
                    }, 
                    {
                    path: '/home/Contentdetection/detection',
                    component: './home/Contentdetection/detection/index.js',
                }]
            }, {
                path: '/home/Groupdetection',
                component: './home/Groupdetection/index.js',
            }, {

                path: '/home/Associationanalysis',
                component: './home/Associationanalysis/index.js',

            },
            {

                path: '/home/Analysis',
                component: './home/Analysis/index.js',

            }, {

                path: '/home/Collectionconfiguration',
                component: './home/Collectionconfiguration/index.js',

            }, {

                path: '/home/Systemconfiguration',
                component: './home/Systemconfiguration/index.js',

            }]
        }
    ],
    plugins: [
        // ref: https://umijs.org/plugin/umi-plugin-react.html
        ['umi-plugin-react', {
            antd: true,
            dva: true,
            dynamicImport: false,
            title: 'item2',
            dll: false,
            locale: {
                default: 'zh-CN', //默认语言 zh-CN
                baseNavigator: true, // 为true时，用navigator.language的值作为默认语言
                antd: true // 是否启用antd的<LocaleProvider />
            },
            routes: {
                exclude: [
                    /models\//,
                    /services\//,
                    /model\.(t|j)sx?$/,
                    /service\.(t|j)sx?$/,
                    /components\//,
                ],
            },
        }],
    ],
    cssLoaderOptions: {
        localIdentName: '[local]'
    },
    proxy: {
        '/api': {
            target: 'http://localhost:3000',
            pathRewrite: {
                '^/api': ''
            },
            changeOrigin: true
        }
    }
}
